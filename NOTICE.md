
The following files were taken from the scheduler-plugins project and modified where necessary (https://github.com/kubernetes-sigs/scheduler-plugins). 
scheduler-plugins is licensed under the Apache License 2.0.

* `hack/**/*.sh`
* `hack/openapi-violation.list`
* `hack/tools.go`
* `build/scheduler/Dockerfile`
* `Makefile`

