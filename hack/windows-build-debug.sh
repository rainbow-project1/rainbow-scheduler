#!/usr/bin/env bash
# This script allows building a debug version of the scheduler using Git Bash on Windows x64.
# The script must be run from the rainbow-scheduler workspace's root directory, i.e., `./hack/windows-build-debug.sh`.
# The outcome is similar to running `make build-scheduler-debug` on Linux.

set -x
set -o errexit

COMMONENVVAR="GOOS=windows GOARCH=amd64"
BUILDENVVAR=CGO_ENABLED=0
PROJECT_VERSION=0.0.1
RELEASE_VERSION="$(date +%Y%m%d%H%M)-v$PROJECT_VERSION"
VERSION="v$PROJECT_VERSION" #$(echo $RELEASE_VERSION | awk -F - '{print $$2}')

if [ "$1" != "--skip-vendor" ]; then
    ./hack/update-vendor.sh
fi
./hack/update-generated-openapi.sh
env $COMMONENVVAR $BUILDENVVAR go build -gcflags="all=-N -l" -ldflags "-X k8s.io/component-base/version.gitVersion=$VERSION" -o bin/rainbow-scheduler.exe cmd/scheduler/main.go
